import { animate, keyframes, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-angular-animation',
  templateUrl: './angular-animation.component.html',
  styleUrls: ['./angular-animation.component.scss'],
  animations: [
    trigger('sun', [
      state('sunrise', style({
        transform: 'translate(0,0)',
      })),
      state('sunset', style({
        transform: 'translate(-100px, 100px)',
      })),
      transition('sunrise => sunset', [
        animate('1.1s ease')
      ]),
      transition('sunset => sunrise', [
        animate('1.1s 0.4s ease')
      ])
    ]),
  ]
})
export class AngularAnimationComponent implements OnInit {

  isDay = true;

  constructor() { }

  ngOnInit(): void {
  }

  switch() {
    this.isDay = !this.isDay;
  }


}
