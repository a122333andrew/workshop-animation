import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-origin',
  templateUrl: './origin.component.html',
  styleUrls: ['./origin.component.scss']
})
export class OriginComponent implements OnInit {

  isDay = true;

  constructor() { }

  ngOnInit(): void {
  }

  switch() {
    this.isDay = !this.isDay;
  }
}
