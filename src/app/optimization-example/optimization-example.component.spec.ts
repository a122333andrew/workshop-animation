import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OptimizationExampleComponent } from './optimization-example.component';

describe('OptimizationExampleComponent', () => {
  let component: OptimizationExampleComponent;
  let fixture: ComponentFixture<OptimizationExampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OptimizationExampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OptimizationExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
