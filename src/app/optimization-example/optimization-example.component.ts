import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-optimization-example',
  templateUrl: './optimization-example.component.html',
  styleUrls: ['./optimization-example.component.scss']
})
export class OptimizationExampleComponent implements OnInit {

  isDay = true;

  constructor() { }

  ngOnInit(): void {
  }

  switch() {
    this.isDay = !this.isDay;
  }

}
