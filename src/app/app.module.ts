import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularAnimationComponent } from './angular-animation/angular-animation.component';
import { OriginComponent } from './origin/origin.component';
import { OptimizationComponent } from './optimization/optimization.component';
import { OptimizationExampleComponent } from './optimization-example/optimization-example.component';

@NgModule({
  declarations: [
    AppComponent,
    AngularAnimationComponent,
    OriginComponent,
    OptimizationComponent,
    OptimizationExampleComponent
  ],
  imports: [
    BrowserModule,
    // BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
