import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-optimization',
  templateUrl: './optimization.component.html',
  styleUrls: ['./optimization.component.scss']
})
export class OptimizationComponent implements OnInit {

  isDay = true;

  constructor() { }

  ngOnInit(): void {
  }

  switch() {
    this.isDay = !this.isDay;
  }
}
